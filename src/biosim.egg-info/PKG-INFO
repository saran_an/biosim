Metadata-Version: 2.1
Name: biosim
Version: 1.0
Summary: BioSim, Modelling the Ecosystem of Rossumoya.
Home-page: https://gitlab.com/nmbu.no/emner/inf200/h2021/january-teams/a48_saranjan_pradeep/biosim-a48-saranjan-pradeep
Author: attr: biosim.__author__
Author-email: attr: biosim.__email__
License: MIT License
Project-URL: Bug Tracker, https://gitlab.com/nmbu.no/emner/inf200/h2021/january-teams/a48_saranjan_pradeep/biosim-a48-saranjan-pradeep
Platform: UNKNOWN
Classifier: Programming Language :: Python :: 3
Classifier: License :: OSI Approved :: MIT License
Classifier: Operating System :: OS Independent
Classifier: Development Status :: 4 - Beta
Classifier: Environment :: Console
Classifier: Intended Audience :: Science/Research
Classifier: Topic :: Scientific/Engineering
Requires-Python: >=3.8
Description-Content-Type: text/markdown
License-File: LICENSE

# BioSim, Group A48 Saranjan Pradeep

#### Authors: Pradeep Manoraj, Saranjan Anpalagan / NMBU
#### E-mail: pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no

## Preface
Aas, January 2022

This project assignment has been prepared in connection with a course called INF200 taught by
Hans Ekkehard Plesser at the Norwegian University of Life Sciences, NMBU. The course is about
assessing the appropriateness of advanced programs and can ensure the quality of these. Quality
assurance is done by performing custom tests using such as built-in packages. In addition, the
subject learns to look at the development and gain an insight into what responsibilities the
programmer has. This is done by making the correct and reliable decision of your own programs,
quality assurance, and documentation.

## Purpose, Project Description
The purpose of this BioSim project is to initialize an island where herbivores and carnivores live.
Defined by the parameters set, the simulation will visualize where and how many carnivores and
herbivores are living at a certain place at given time (year).

## Information
Rossumøya is a small island in the middle of vast ocean that belongs to the island nation of
Pylandia. The main purpose is to preserve the ecosystem on the Island for future generations.
The Island, Rossumøya, is characterized by landscapes and two species. The landscapes are
Lowland (L), Highland (H), Water (W) and Desert (D). The species are Herbivores
(Plant eaters) and Carnivores (predators). [More Information, Modelling the Ecosystem of Rossumøya (PDF)](https://gitlab.com/nmbu.no/emner/inf200/h2021/inf200-course-materials/-/blob/main/january_block/INF200_H21_BioSimJan_v2.pdf)

## Inspiration, related projects
This project has been greatly inspired by Hans Ekkehard Plesser, professor at NMBU. Some of the
scripts that are included in this project are inspired and created by him.

## Requirements
Normally should dependencies be automatically installed. The following package are needed, for
manual preinstallation of dependencies:
Python modules: numpy, pandas, matplotlib, ffmpeg, magick

## HTML Documentation
To generate the html documentation using Sphinx, issue from the BioSim sourve code directory:

```python
$ cd doc
$ make html
```

The main html file is in _build/html/index.html. Numpydoc and the Sphinx ReadTheDocs theme may be
needed:

```python
$ pip install numpydoc --user
$ pip install sphinx-rtd-theme --user
```

## More information
For more information about the project, Modelling the Ecosystem of Rossumøya can be find at: [GitLab: Group A48 by Pradeep Manoraj, Saranjan Anpalagan](https://gitlab.com/nmbu.no/emner/inf200/h2021/january-teams/a48_saranjan_pradeep/biosim-a48-saranjan-pradeep)


## Figure
[Figure of the Simulation](https://gitlab.com/nmbu.no/emner/inf200/h2021/january-teams/a48_saranjan_pradeep/biosim-a48-saranjan-pradeep/-/blob/main/docs/images/simulation.png)


