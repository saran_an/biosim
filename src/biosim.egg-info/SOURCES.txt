LICENSE
README.md
pyproject.toml
setup.cfg
setup.py
examples/check_sim.py
examples/sample_sim_log.py
examples/sample_sim_movie.py
examples/sim_herbivores_desert.py
src/biosim/__init__.py
src/biosim/animal_count_log.py
src/biosim/animals.py
src/biosim/island.py
src/biosim/landscape.py
src/biosim/simulation.py
src/biosim/visualization.py
src/biosim.egg-info/PKG-INFO
src/biosim.egg-info/SOURCES.txt
src/biosim.egg-info/dependency_links.txt
src/biosim.egg-info/requires.txt
src/biosim.egg-info/top_level.txt