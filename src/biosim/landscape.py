# -*- coding: utf-8 -*-

"""
This module contains the necessary information about the characteristics of landscape-types on the
island. The module consists of a main class called "Landscape" as well as
4 subclasses: Lowland, Highland, Desert and Water.
The main class deals with the characteristic features that are available for all landscape types,
but the respective use of methods are dependent on parameter values and habitable states. Water has
access to all the methods, but since it is not habitable, the methods for that subclass will not
be called upon in simulation. See methods in "island"-module for more information.

**Purpose**

Purpose of this module is to give detailed information about the respective landscape types
and their characteristics.

"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"


import random

from .animals import Herbivore
from .animals import Carnivore
import itertools
import copy


class Landscape:
    """
    Specific location/cell on the map.
    This is the base class for certain landscape-types as subclasses.
    """

    # Parameters defined at class level
    params = {
        "f_max": 0
    }

    @classmethod
    def set_params(cls, new_params):
        """
        Change the parameters of location

        Parameters
        ----------
        new_params: dict
            Dictionary that contains new parameters for the landscape
        """
        for key in new_params.keys():
            if key not in cls.params:
                raise KeyError(f"Invalid parameter name: {key}")

            if cls.__name__ == "Lowland" and new_params[key] < 0:
                raise ValueError(f"{key} cannot be negative")
            elif cls.__name__ == "Highland" and new_params[key] < 0:
                raise ValueError(f"{key} cannot be negative")
            elif cls.__name__ not in ['Lowland', 'Highland']:
                raise ValueError(f"{cls.__name__} cannot change parameter value")

        cls.params.update(new_params)

    @classmethod
    def get_params(cls):
        """
        Get class parameters

        Returns
        -------
        param: dict
            Dictionary with class parameters
        """
        return cls.params

    def __init__(self):
        """
        Constructor that initiates an instance of class Landscape.
        """
        self.herbivores = []
        self.carnivores = []
        self.f_available = 0

    def get_num_herbivores(self):
        """
        Returns
        -------
        int
            Number of herbivores in location
        """
        return len(self.herbivores)

    def get_num_carnivores(self):
        """
        Returns
        -------
        int
            Number of carnivores in location
        """
        return len(self.carnivores)

    def get_num_pr_species(self):
        return {'Herbivore': len(self.herbivores), 'Carnivore': len(self.carnivores)}

    def get_ages_pr_species(self):
        """
        Returns
        -------
        dict
            Dictionary containing list of ages of all animals in location, seperated by species
        """
        return {'Herbivore': [herbivore.age for herbivore in self.herbivores],
                'Carnivore': [carnivore.age for carnivore in self.carnivores]}

    def get_weight_pr_species(self):
        """
        Returns
        -------
        dict
            Dictionary containing list of weights of all animals in location, seperated by species
        """
        return {'Herbivore': [herbivore.weight for herbivore in self.herbivores],
                'Carnivore': [carnivore.weight for carnivore in self.carnivores]}

    def add_population(self, animals):
        """
        Add population of animals to location.

        Parameters
        ----------
        animals: list
            List containing dictionaries with specific keys (species, age, weight)
            and their respective values to describe specifications of animals to be set in location.

        """
        species = ['Herbivore', 'Carnivore']
        for animal in animals:
            if animal['species'] not in species:
                raise KeyError(f"Invalid species: {animal['species']}")

            if animal['species'] == "Herbivore":
                self.herbivores.append(Herbivore(ini_age=animal['age'],
                                                 ini_weight=animal['weight']))
            elif animal['species'] == "Carnivore":
                self.carnivores.append(Carnivore(ini_age=animal['age'],
                                                 ini_weight=animal['weight']))

    def regrowth_fodder(self):
        """
        Renew the fodder available in location to the max amount.
        """
        self.f_available = self.params["f_max"]

    def herbivores_eat(self):
        """
        Herbivores tries to eat fodder available at location, in descending order of fitness.
        """
        for herbivore in self.herbivores:
            herbivore.fitness_update()

        herbivore_sort = sorted(self.herbivores, key=lambda animal: animal.fitness, reverse=True)
        eat_max = Herbivore.params['F']

        for herbivore in herbivore_sort:
            if self.f_available == 0:
                break
            if self.f_available >= eat_max:
                herbivore.weight_gain()
                self.f_available -= eat_max
            elif self.f_available > 0:
                herbivore.weight_gain(self.f_available)
                self.f_available = 0

    def carnivores_eat(self):
        """
        Carnivores tries to eat herbivores with the lowest fitness, in random order.
        """
        for herbivore in self.herbivores:
            herbivore.fitness_update()

        random.shuffle(self.carnivores)
        herbivores_surviving = copy.copy(self.herbivores)
        herbivores_dead = []
        eat_max = Carnivore.params['F']

        for carnivore in self.carnivores:
            if len(herbivores_surviving) <= 0:
                break
            eaten = 0
            herbivore_sort = sorted(herbivores_surviving,
                                    key=lambda animal: animal.fitness,
                                    reverse=False)
            for herbivore in herbivore_sort:
                if eaten >= eat_max:
                    break
                if carnivore.hunting_kill(herbivore_phi=herbivore.fitness):
                    meal_portion = herbivore.weight
                    if eaten + meal_portion >= eat_max:
                        meal_portion = eat_max - eaten
                    carnivore.weight_gain(meal_portion)
                    eaten += meal_portion
                    herbivores_dead.append(herbivore)

            herbivores_surviving = [survived_herbivore
                                    for survived_herbivore in herbivores_surviving
                                    if survived_herbivore not in herbivores_dead]
            herbivores_dead = []

        self.herbivores = herbivores_surviving

    def procreation(self):
        """
        Add newborn to the population of species in location if animal is giving birth.
        """
        def newborn(population):
            """
            Newborn(s) given amount of population and the animal's ability to give birth.
            Parameters
            ----------
            population: list
                List containing instances of a animal class.

            Returns
            -------
            list
                List containing instances of offsprings of given animal class.
            """
            num_pop = len(population)
            return [offspring for parent in population if (offspring := parent.birth(num_pop))]

        if self.get_num_herbivores() >= 2:
            self.herbivores.extend(newborn(self.herbivores))
        if self.get_num_carnivores() >= 2:
            self.carnivores.extend(newborn(self.carnivores))

    def emigration(self):
        """
        Separate emigrating animals from staying animals.
        Returns
        -------
        list
            List containing emigrating animals.
        """
        for _ in itertools.chain(self.herbivores, self.carnivores):
            _.fitness_update()

        def emigrating(population):
            """
            Animals that moves out of location
            Parameters
            ----------
            population: list
                List containing instances of a species class.

            Returns
            -------
            list
                List containing emigrating animals
            """
            return [animal for animal in population
                    if animal.emigration() and animal.migrated is False]

        return emigrating(self.herbivores), emigrating(self.carnivores)

    def add_immigrated_animal(self, animal):
        """
        Add immigrating animals to the populations of species in location.
        """
        animal.migrated = True
        if isinstance(animal, Herbivore):
            self.herbivores.append(animal)
        elif isinstance(animal, Carnivore):
            self.carnivores.append(animal)

    def reset_migration(self):
        """
        Make all the animals in location to reset their moved-status to no(False),
        so they can move in next cycle.
        """
        for animal in itertools.chain(self.herbivores, self.carnivores):
            animal.reset_migration()

    def aging(self):
        """
        Age all the animals in location by one year.
        """
        for animal in itertools.chain(self.herbivores, self.carnivores):
            animal.age_update()

    def weight_loss(self):
        """
        Loss of weight for every animal in location
        """
        for animal in itertools.chain(self.herbivores, self.carnivores):
            animal.weight_loss()

    def death(self):
        """
        Remove the dead animals from population in the location.
        """
        for _ in itertools.chain(self.herbivores, self.carnivores):
            _.fitness_update()

        def survivors(population):
            """
            Surviving animals after a cycle
            Parameters
            ----------
            population: list
                List containing instances of a species class.

            Returns
            -------
            list
                Surviving animals in population
            """

            return [animal for animal in population if not animal.death()]

        self.herbivores = survivors(self.herbivores)
        self.carnivores = survivors(self.carnivores)


class Lowland(Landscape):
    """
    Landscape type: Lowland
    Inherits from main class Landscape
    """

    params = {
        "f_max": 800.0
    }
    habitable = True

    def __init__(self):
        """
        Constructor that initiates class instances of Lowland.
        """
        super().__init__()


class Highland(Landscape):
    """
    Landscape type: Highland
    Inherits from main class Landscape
    """

    params = {
        "f_max": 300.0
    }
    habitable = True

    def __init__(self):
        """
        Constructor that initiates class instances of Highland.
        """
        super().__init__()


class Desert(Landscape):
    """
    Landscape type: Desert
    Inherits from main class Landscape
    """
    habitable = True

    def __init__(self):
        """
        Constructor that initiates class instances of Desert.
        """
        super().__init__()


class Water(Landscape):
    """
    Landscape type: Water
    Inherits from main class Landscape
    """
    habitable = False

    def __init__(self):
        """
        Constructor that initiates class instances of Water.
        """
        super().__init__()
