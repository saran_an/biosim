"""
Template for BioSim class.
This module deals with the simulation of the Island, Russomøya. The simulation is based
for a given number of years, and the visualization is based on the module "visualization.py".
By running this module, a graphical window will appear with various subplots
as a map of geography landscape with different colorcode, distribution
heat maps that shows how many animals per species there are in each cell
and line graph of the total number of animals.

Inspired by Hans Plesser Ekkehard, 2022. Lecture:

* ../randvis/graphics.py
* ../randvis/simulation.py

**Notice**

To be able to run this module, one depends on the following packages being installed as a result of
python enviroment: "pandas".

"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2021 Hans Ekkehard Plesser / NMBU

from .animals import Herbivore, Carnivore
from .landscape import Highland, Lowland
from .island import Island
from .visualization import Visualization
from .animal_count_log import Log

import random
import numpy as np
import pandas as pd
import textwrap


class BioSim:
    """
    BioSim is the base class.
    """
    def __init__(self, island_map, ini_pop, seed,
                 vis_years=1, ymax_animals=None, cmax_animals=None, hist_specs=None,
                 img_dir=None, img_base=None, img_fmt='png', img_years=None,
                 log_file=None):

        """
        Parameters
        ----------
        island_map: str
            Multi-line string specifying island geography
        ini_pop: list
            List of dictionaries specifying initial population
        seed: int
            Integer used as random number seed
        vis_years: int
            years between visualization updates (if 0, disable graphics)
        ymax_animals: int
            Number specifying y-axis limit for graph showing animal numbers
        cmax_animals: dict
            Dict specifying color-code limits for animal densities
        hist_specs: dict
            Specifications for histograms, see below
        img_dir: str
            String with path to directory for figures
        img_base: str
            String with beginning of file name for figures
        img_fmt: str
            String with file type for figures, e.g. 'png'
        img_years:
            years between visualizations saved to files (default: vis_years)
        log_file
            If given, write animal counts to this file.

        Notes
        -------
        If ymax_animals is not given, the y-axis limit will be adjusted automatically.
        If cmax_animals is not given, fixed default values will be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,

        ::

           {'Herbivore': 50, 'Carnivore': 20}

        hist_specs is a dictionary with one entry per property for which a histogram shall be shown.
        For each property, a dictionary providing the maximum value and the bin width must be
        given, e.g.,

        ::

            {'weight': {'max': 80, 'delta': 2}, 'fitness': {'max': 1.0, 'delta': 0.05}}

        Permitted properties are 'weight', 'age', 'fitness'.

        If img_dir is None, no figures are written to file. Filenames are formed as

        ::

            f'{os.path.join(img_dir, img_base}_{img_number:05d}.{img_fmt}'

        where img_number are consecutive image numbers starting from 0.

        img_dir and img_base must either be both None or both strings.
        """

        random.seed(seed)
        np.random.seed(seed)

        # the following will be initialized by _setup_graphics
        self.island_map = textwrap.dedent(island_map)
        self.ini_pop = ini_pop
        self.island = Island(island_map=self.island_map, ini_pop=self.ini_pop)
        self.vis_years = vis_years
        self.ymax_animals = ymax_animals
        self.cmax_animals = cmax_animals

        if ymax_animals is None:
            self.ymax_animals = 25000

        if cmax_animals is None:
            self.cmax_animals = {"Herbivore": 150, "Carnivore": 90}

        if hist_specs is None:
            self.hist_specs = {"weight": {"max": 60, "delta": 2},
                               "fitness": {"max": 1.0, "delta": 0.05},
                               "age": {"max": 60, "delta": 2}, }
        else:
            self.hist_specs = hist_specs

        if img_years is None:
            self.img_years = self.vis_years
        else:
            self.img_years = img_years

        self.log_file = log_file
        if self.log_file is not None:
            self.log = Log(self.log_file)

        self._visual = Visualization(self.cmax_animals, self.hist_specs, self.img_years,
                                     img_dir, img_base, img_fmt)

        self._current_year = 0
        self.count_year = 0
        self._final_year = None

    @staticmethod
    def set_animal_parameters(species, params):
        """
        Set parameters for animal species.

        Parameters
        ----------
        species: str
            Name of animal species
        params: dict
            Dict with valid parameter specification for species

        Examples
        ---------
        ::
            Biosim().set_animal_parameters('Herbivore', {w_birth: 10.0})
        """
        if species == "Herbivore":
            Herbivore.set_params(params)
        elif species == "Carnivore":
            Carnivore.set_params(params)
        else:
            raise ValueError("The species must be either Herbivore or Carnivore")

    @staticmethod
    def set_landscape_parameters(landscape, params):
        """
         Set parameters for landscape type.

        Parameters
        ----------
        landscape: String
            String, code letter for landscape
        params: Dict
            Dict with valid parameter specification for landscape

        Examples
        ---------
        ::

            BioSim.set_landscape_parameters("L", {f_max: 1000})
        """
        if landscape == "L":
            Lowland.set_params(params)
        elif landscape == "H":
            Highland.set_params(params)

    def simulate(self, num_years):
        """
        Simulation for the visualizing of result.

        Parameters
        ----------
        num_years: int
            number of years to simulate

        """
        self._final_year = self._current_year + num_years
        if self.vis_years != 0:
            self._visual.graph_window(self._final_year, self.ymax_animals, self._current_year)
            self._visual.map_graph(self.island_map)

            if self.img_years % self.vis_years != 0:
                raise ValueError("img_year must be multiple of vis_year")

        while self._current_year < self._final_year:
            self.island.annual_cycle()
            self._current_year += 1

            if self.log_file is not None:
                self.log.update(self._current_year, self.num_animals_per_species)

            if self.vis_years != 0 and self._current_year % self.vis_years == 0:
                self._visual.update_graph(
                    self._current_year,
                    self.distributions,
                    self.num_animals_per_species,
                    self.island.species_attributes[0],
                    self.island.species_attributes[1],
                )

    def add_population(self, population):
        """
        Add a population to the island
        Parameters
        ----------
        population: list
            List of dictionaries specifying population

        Examples
        ---------
        ::

            BioSim.add_population([{'loc': (10, 10),'pop': [{'species': 'Carnivore','age': 5,
            'weight': 20} for _ in range(40)]}])

        """
        self.island.add_population(population)

    def make_movie(self, movie_fmt=None):
        """
        Creates MPEG4 movie from visualization images saved.

        Parameters
        ----------
        movie_fmt: str
            Format for the generating movie

        """
        self._visual.make_movie(movie_fmt)

    @property
    def year(self):
        """Last year simulated."""

        return self._current_year

    @property
    def num_animals(self):
        """Total number of animals on island."""

        return self.island.get_num_population()

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary."""

        return self.island.get_num_pr_species()

    @property
    def distributions(self):
        """
        Make a DataFrame to use in the visualization for heat map

        Returns
        -------
        distribution
            A DataFrame for the species heat map visualization
        """
        cell_data = []

        for location, cell in self.island.island_map.items():
            row = location[0]
            col = location[1]
            num_herbs = cell.get_num_herbivores()
            num_carns = cell.get_num_carnivores()
            cell_data.append([row, col, num_herbs, num_carns])
        distribution = pd.DataFrame(data=cell_data,
                                    columns=["Row", "Col", "Herbivore", "Carnivore"])
        return distribution
