# -*- coding: utf-8 -*-

"""
This module, animals.py, contains the necessary functions and methods for give a detailed
information on the evolution of the animals. The module consists of a main class called "Aniaml"
as well as two subclasses: Herbivore and Carnivore. The main class deals with the characteristic
features that are common to both animals. This can be, for example, how fitness for the animal
is to be calculated using the weight and age of the animal. The subclasses were created because they
behave differently in certain instances.

**Purpose**

Purpose of this module is to give detailed information about the animals and their
behaviors on the Island.

"""
import math
import random

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"


class Animal:
    """
    Animal class is the base class for certain species as subclasses.
    The parameters are:

    * **w_birth:** Parameter, mean for the birth weight
    * **sigma_birth:** Parameter, standard deviation for the birth weight
    * **beta:** Parameter, the weight of the animal increases by beta * F
    * **eta:** Parameter, the weight of the animal decreases by eta * weight
    * **a_half:** Parameter, used in the calculation of the fitness of animal
    * **phi_age:** Parameter, growth rate in the fitness calculation
    * **w_half:** Parameter, used in the calculation of the fitness of animal
    * **phi_weight:** Parameter, growth rate in the fitness calculation
    * **mu:** Parameter, used in the calculation of probability of migration
    * **gamma:** Parameter, used in the calculation of the probability to give birth in a year
    * **zeta:** Parameter, used in the calculation of the probability of birth
    * **xi:** Parameter, used in the calculation of parent animal's loss of weight
    * **omega:** Parameter, used to calculate the probability of the animal death
    * **F:** Parameter, an amount of maximal fodder the animal tries to eat
    * **DeltaPhiMax:** Parameter, used to calculate the probability of carnivore killing herbivore
    """

    params = {
        "w_birth": None,
        "sigma_birth": None,
        "beta": None,
        "eta": None,
        "a_half": None,
        "phi_age": None,
        "w_half": None,
        "phi_weight": None,
        "mu": None,
        "gamma": None,
        "zeta": None,
        "xi": None,
        "omega": None,
        "F": None,
        "DeltaPhiMax": None
    }

    @classmethod
    def set_params(cls, new_params):
        """
        Change the parameters of the species

        Parameters
        ----------
        new_params: dict
            Dictionary that contains new parameters for the species
            Legal keys: 'w_birth', 'sigma_birth', 'beta', 'eta', 'a_half', 'phi_age', 'w_half',
            'phi_weight', 'mu', 'gamma', 'zeta', 'xi', 'omega', 'F' and 'DeltaPhiMax'.
        Raises
        ------
        ValueError
            Raises if all parameter values are not positive(>= 0), expect for.
        KeyError
            Raises if the parameter name is invalid.
        """

        for key in new_params.keys():
            if key not in cls.params:
                raise KeyError(f"Invalid parameter name: {key}")

            if cls.__name__ == "Carnivore" and key == "DeltaPhiMax" and new_params[key] <= 0:
                raise ValueError("DeltaPhiMax must be strictly positive!")
            elif key == "eta" and new_params[key] > 1:
                raise ValueError("eta must be less or equal to 1")
            elif key != "DeltaPhiMax" and key != "eta" and new_params[key] < 0:
                raise ValueError(f"{key} must be strictly positive")
            cls.params[key] = new_params[key]

    @classmethod
    def get_params(cls):
        """
        Get class parameters

        Returns
        -------
        param: dict
            Dictionary with class parameters
        """

        return cls.params

    def __init__(self, ini_age=0, ini_weight=None):
        """
        Constructor that initiates an instance of class Animals.

        Parameters
        ----------
        ini_age: int [default = 0]
            Sets the age of a new instance of a species. The default value is set to be 0.
        ini_weight: int/float [default = None]
            Sets the weight of a new instance of a species.
            If no value given, the default weight is than drawn from a Gaussian distribution
            based on the mean and standard deviation in class parameters.

        """

        if type(ini_age) == float and not ini_age.is_integer():
            raise ValueError("Age must be a whole number.")
        if ini_age < 0:
            raise ValueError("Age must be positive!")
        self._age = int(ini_age)

        if ini_weight is not None and ini_weight <= 0:
            raise ValueError("Weight must be strictly positive!")
        if ini_weight is None:
            birth_w = random.gauss(self.params['w_birth'], self.params['sigma_birth'])
            if birth_w < 0:
                birth_w = 0
            self._weight = birth_w

        else:
            self._weight = ini_weight

        self._phi = None
        self.fitness_update()
        self.migrated = False

    @property
    def age(self):
        """
        Property function give access to the animals age.

        Returns
        -------
        _age: float
            Current age of instance of specific animal.
        """
        return self._age

    def age_update(self):
        """ Grow animal by one year."""
        self._age += 1

    @property
    def weight(self):
        """
        Property function give access to the animals weight.
        Animals are born with weight:

        Returns
        -------
        _weight: float
            Current weight of instance of specific animal.
        """
        return self._weight

    def weight_gain(self, F_tilde=None):
        """
        Update weight of animal after fodder eaten.

        Parameters
        ----------
        F_tilde: float
            Amount of fodder which is less than F amount of fodder, animals tries to eat each year.
        """
        beta = self.params['beta']
        F = self.params['F']
        if F_tilde is not None:
            self._weight += beta * F_tilde
        else:
            self._weight += beta * F

    def weight_loss(self):
        """ The natural weight loss an animal goes through every year."""
        eta = self.params['eta']
        self._weight -= eta * self._weight

    @property
    def fitness(self):
        """
        Property function give access to the animals fitness value.

        Returns
        -------
        _phi: float
            Current fitness value of instance of specific animal.
        """
        return self._phi

    def fitness_update(self):
        r"""
        Update the fitness-level according to current age, weight and parameter values.

        The fitness is calculated based on age and weight using the following formula:

        .. math::
            \begin{equation}
            \Phi =
            \begin{cases}
             0 & w \leq 0 \\
            q^+(a, a_{\frac{1}{2}}, \phi_{age}) \times q^-(w, w_{\frac{1}{2}},
            \phi_{weight}) & else
            \end{cases}
            \end{equation}

        where

        .. math::
            \begin{equation}
            q^\pm(x, x_{\frac{1}{2}}, \phi) =
            \frac{1}{1 + e^{\pm \phi(x - x_{\frac{1}{2}})}}
            \end{equation}

        Note that :math:`0 \leq \Phi \leq 1`.

        """
        e = math.e
        phi_age = self.params['phi_age']
        a_half = self.params['a_half']
        phi_weight = self.params['phi_weight']
        w_half = self.params['w_half']
        self._phi = (1 / (1 + e ** (phi_age * (self._age - a_half)))) * \
                    (1 / (1 + e ** (-phi_weight * (self._weight - w_half))))

    def emigration(self):
        """
        Determine whether the animal can move to another location.

        Returns
        -------
        bool
            True, if animal is can emigrate
            False, if animal cannot emigrate
        """

        move_prob = self.params['mu'] * self._phi
        return random.random() < move_prob

    def reset_migration(self):
        """
        Reset the animals moved status back to False.
        """
        self.migrated = False

    def birth(self, num_species):
        r"""
        Determines whether the animal is giving birth or not based on certain conditions,
        and makes a baby if the conditions are met.

        The probability to give birth to an offspring in a year is given by:

        .. math::
            \begin{equation}
            min(1, \gamma \times \Phi \times (N-1))
            \end{equation}

        where N is the number of animals of the same species in the cell.

        The probability of birth is zero when the weight is:

        .. math::
            \begin{equation}
            w < \zeta(w_{birth} + \sigma_{birth})
            \end{equation}

        Parameters
        ----------
        num_species: int
            Number of animals of that species in the same location/cell.

        Returns
        -------
        new_baby: object
            An instance of class with default argument values, if the conditions of birth are met.
        bool
            False, if the conditions of birth is not met
        """
        zeta = self.params['zeta']
        w_birth = self.params['w_birth']
        sigma_birth = self.params['sigma_birth']
        gamma = self.params['gamma']
        xi = self.params['xi']

        if self._weight >= zeta * (w_birth + sigma_birth):

            birth_prob = min(1, gamma * self._phi * (num_species - 1))
            if random.random() < birth_prob:
                new_baby = type(self)()
                weight_loss = new_baby._weight * xi
                if 0 <= weight_loss <= self._weight:
                    self._weight -= weight_loss
                    return new_baby
                else:
                    return None
            else:
                return None
        else:
            return None

    def death(self):
        r"""
        Determines whether the animal dies or not, based on the parameter 'omega' and fitness level.

        .. math::
            \begin{equation}
            \omega(1 - \Phi)
            \end{equation}

        Returns
        -------
        bool
            True, if animal survives
            False, if animal dies
        """
        if self._weight == 0:
            return True

        prob_death = self.params['omega'] * (1 - self._phi)
        return random.random() < prob_death


class Herbivore(Animal):
    """
    A subclass of Animal for the Herbivore species.
    Inherits the attributes and methods from the main class.
    """
    params = {
        "w_birth": 8.0,
        "sigma_birth": 1.5,
        "beta": 0.9,
        "eta": 0.05,
        "a_half": 40.0,
        "phi_age": 0.6,
        "w_half": 10.0,
        "phi_weight": 0.1,
        "mu": 0.25,
        "gamma": 0.2,
        "zeta": 3.5,
        "xi": 1.2,
        "omega": 0.4,
        "F": 10.0,
    }

    def __init__(self, ini_age=0, ini_weight=None):
        """
        Constructor that initiates class instances of Herbivore.

        Parameters
        ----------
        ini_age: int [default = 0]
            Sets the age of a new instance of a species. The default value is set to be 0.
        ini_weight: int/float [default = None]
            Sets the weight of a new instance of a species.
            If no value given, the default weight is than drawn from a Gaussian distribution
            based on the mean and standard deviation in class parameters.
        """
        super().__init__(ini_age, ini_weight)


class Carnivore(Animal):
    """
    A subclass of Animal for the Carnivore species.
    Inherits the attributes and methods from the main class.
    """

    params = {
        "w_birth": 6.0,
        "sigma_birth": 1.0,
        "beta": 0.75,
        "eta": 0.125,
        "a_half": 40.0,
        "phi_age": 0.3,
        "w_half": 4.0,
        "phi_weight": 0.4,
        "mu": 0.4,
        "gamma": 0.8,
        "zeta": 3.5,
        "xi": 1.1,
        "omega": 0.8,
        "F": 50.0,
        "DeltaPhiMax": 10.0
    }

    def __init__(self, ini_age=0, ini_weight=None):
        """
        Constructor that initiates class instances of Carnivore.

        Parameters
        ----------
        ini_age: int [default = 0]
            Sets the age of a new instance of a species. The default value is set to be 0.
        ini_weight: int/float [default = None]
            Sets the weight of a new instance of a species.
            If no value given, the default weight is than drawn from a Gaussian distribution
            based on the mean and standard deviation in class parameters.
        """
        super().__init__(ini_age, ini_weight)

    def hunting_kill(self, herbivore_phi):
        r"""
        Determine whether the specific carnivore will kill a specific herbivore
        based on their fitness levels.

        Carnivores will kill a herbivore with probability

         .. math::
            \begin{equation}
            p =
            \begin{cases}
             0 & if\;  \Phi_{carn} \leq \Phi_{herb}\\
             \frac{\Phi_{carn} - \Phi_{herb}}{\Delta\Phi_{max}} & if\; 0 < \Phi_{carn} -
             \Phi_{herb} < \Delta\Phi_{max}\\
            1 & otherwise.
            \end{cases}
            \end{equation}

        Parameters
        ----------
        herbivore_phi: float
            Fitness-level of an instance of Herbivore

        Returns
        -------
        bool
            True, if the carnivore would kill
            False, if the carnivore fails to kill
        """
        delta_phi_max = self.params['DeltaPhiMax']
        if self._phi <= herbivore_phi:
            return False
        elif 0 < (self._phi - herbivore_phi) < delta_phi_max:
            kill_probability = (self._phi - herbivore_phi) / delta_phi_max
            return random.random() < kill_probability
        else:
            return True
