# -*- coding: utf-8 -*-

"""
This script deals with log_file, where it is based on a class with a function.
If given, a log file will be created and it will write animal counts to this file.
The function deals with the method of creating a file, writing in it and updating this file.

References
----------


"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import csv


class Log:
    """Creates a log file"""
    def __init__(self, log_file):
        """
        Initiates Log Class
        Parameters
        ----------
        log_file: csv-file
            File to write animal counts to
        """

        self.log_file = log_file
        with open(self.log_file, mode='a+') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow(['Year', 'Herbivores', 'Carnivores'])

    def update(self, current_year, num_animals_per_species):
        """
        Updates log_file with animal count for the respective years simulated
        Parameters
        ----------
        current_year: int
            Current year simulated
        num_animals_per_species: dict
            Dictionary containing number of animals per valid species

        """

        herbivore_count = num_animals_per_species['Herbivore']
        carnivore_count = num_animals_per_species['Carnivore']
        with open(self.log_file, mode='a', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow([current_year, herbivore_count, carnivore_count])
