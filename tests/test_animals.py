# -*- coding: utf-8 -*-

"""
Test set for Animal class.

This set of tests checks of the Animal class to be provided by
the simulation module of the biosim package.
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

from biosim.animals import Animal, Herbivore, Carnivore
import scipy.stats as stats
import pytest
import numpy as np

SEED = 876  # Random seed for tests
ALPHA = 0.05  # Significance level for statistical tests


@pytest.fixture(autouse=True)
def default_set_params():
    """Sets the parameters for species to the default values in tests"""
    yield
    Herbivore.set_params(
        {
            "w_birth": 8.0,
            "sigma_birth": 1.5,
            "beta": 0.9,
            "eta": 0.05,
            "a_half": 40.0,
            "phi_age": 0.6,
            "w_half": 10.0,
            "phi_weight": 0.1,
            "mu": 0.25,
            "gamma": 0.2,
            "zeta": 3.5,
            "xi": 1.2,
            "omega": 0.4,
            "F": 10.0,
        }
    )

    Carnivore.set_params(
        {
            "w_birth": 6.0,
            "sigma_birth": 1.0,
            "beta": 0.75,
            "eta": 0.125,
            "a_half": 40.0,
            "phi_age": 0.3,
            "w_half": 4.0,
            "phi_weight": 0.4,
            "mu": 0.4,
            "gamma": 0.8,
            "zeta": 3.5,
            "xi": 1.1,
            "omega": 0.8,
            "F": 50.0,
            "DeltaPhiMax": 10.0
        }
    )


def test_change_set_params():
    """Possibility to change the parameters of Animal class"""
    params = Herbivore.params
    new_params = {
        "w_birth": 12.0,
        "sigma_birth": 1.3,
        "beta": 0.94,
        "eta": 0.15,
        "a_half": 20.0,
        "phi_age": 0.34,
        "w_half": 43.0,
        "phi_weight": 0.24,
        "mu": 0.14,
        "gamma": 0.3,
        "zeta": 3.1,
        "xi": 1.2,
        "omega": 1.8,
        "F": 50.0,
    }
    new_params = Herbivore.set_params(new_params)
    assert new_params != params


def test_get_params():
    new_param = {
        "w_birth": 3.0,
        "sigma_birth": 2.0,
        "beta": 0.45,
        "eta": 0.15,
        "a_half": 50.0,
        "phi_age": 0.332,
        "w_half": 2.0,
        "phi_weight": 0.3,
        "mu": 0.14,
        "gamma": 0.38,
        "zeta": 13.5,
        "xi": 1.12,
        "omega": 20.8,
        "F": 43.0,
        "DeltaPhiMax": 11.0
    }
    Carnivore.set_params(new_param)
    assert Carnivore.get_params() == new_param


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_init(species):
    """ Check that default is callable """
    instance = species()
    assert isinstance(instance, species)


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_update_parameter(species):
    """Check the function to update the parameter values with other values"""
    animal = species()
    pres_param = animal.params
    param = {"a_half": 100, "F": 2}
    new_param = animal.set_params(param)
    assert new_param is not pres_param


def test_param_value_error_DeltaPhiMax():
    """Check that setting invalid value to DeltaPhiMax raises a ValueError"""
    new_param_DeltaPhiMax = {"DeltaPhiMax": 0}
    with pytest.raises(ValueError):
        Carnivore.set_params(new_param_DeltaPhiMax)


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
@pytest.mark.parametrize('param', [{"eta": 3}, {"F": -2}])
def test_param_value_error(species, param):
    """ Check taht setting invalid value raises ValueError for parameters Eta and F. """
    with pytest.raises(ValueError):
        species.set_params(param)


def test_set_params_key_error():
    """Check if a parameter that doesn't exist adds, will raise KeyError."""
    new_param = {"birth": 12.0}
    with pytest.raises(KeyError):
        Animal.set_params(new_param)


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_age(species):
    """ Check the possibility to get and set age of Herbivores and Carnivores"""
    animal = species(5, 12)
    age = 5
    assert animal.age == age


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_age_update(species):
    """ Check if the Herbivore get one year older for each time """
    animal = species(5, 10)
    animal.age_update()
    assert animal.age == 6


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_age_value_error(species):
    """ Test that negative age, raises ValueError"""
    with pytest.raises(ValueError):
        species(ini_age=-3)


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_age_value_error_float(species):
    """ Test age that is not a whole number, raises ValueError"""
    with pytest.raises(ValueError):
        species(ini_age=2.3)


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_weight(species):
    """ Check the possibility to get and set weight of Herbivores and Carnivores"""
    animal = species(5, 10)
    weight = 10
    assert animal.weight == weight


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_weight_value_error(species):
    """ Check if animal class raises ValueError when weight is negative"""
    with pytest.raises(ValueError):
        species(ini_weight=-4)


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_weight_gain(species):
    """Check if the weight of animal update after fodder eaten"""
    animal = species(10, 20)
    F = animal.params['F']
    beta = animal.params['beta']
    assert animal.weight == 20

    animal.weight_gain()
    assert animal.weight == beta * F + 20


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_weight_gain_with_def_F_tilde(species):
    """ Check that Herbivores gain the right amount of weight, by given F_tilde"""
    animal = species(10, 20)
    F_tilde = 5
    beta = animal.params['beta']

    animal.weight_gain(F_tilde)
    assert animal.weight == beta * F_tilde + 20


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_weight_loss(species):
    """Check if weight loss decreases with eta"""
    weight = 20
    animal = species(10, weight)
    eta = species.params['eta']

    animal.weight_loss()
    assert animal.weight == weight - weight * eta


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_fitness(species):
    """
    Check if the fitness function calculate as it should

    Age = 10
    Weight = 20
    Animal.weight = Animal.params["phi_weight"]
    Animal.age = Animal.params["phi_age"]

    Animal.fitness == 0.25
    """
    age = 10
    weight = 20
    species.set_params({"a_half": age, "w_half": weight})
    animal = species(age, weight)
    assert round(animal.fitness, 2) == 0.25


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_death(species):
    """
    This test is "deterministic": We set death probability to 100,
    thus the Animals must always die. We call death() multiple times to
    test this.
    """
    param = ({'omega': 1000.0})
    species.set_params(param)
    animal = species(ini_age=5, ini_weight=20)

    for _ in range(50):
        assert animal.death()


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_death_weight(species):
    """Check if the animal dies if the weight is 0."""
    param = {'eta': 1.0}
    species.set_params(param)
    animal = species(ini_age=5, ini_weight=20)

    animal.weight_loss()
    assert animal.death() is True


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_possible_emigration_true(species, mocker):
    """ Check if the Herbivore and Carnivore has the opportunity to emigrate"""
    mocker.patch('random.random', return_value=0)
    species = species(2, 10)
    assert species.emigration() is True


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_possible_emigration_false(mocker, species):
    """ Check if the Herbivore and Carnivore raise
    False for the emigration method when return_value is 1"""
    mocker.patch('random.random', return_value=1)
    species = species(2, 10)
    assert species.emigration() is False


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_reset_migration(species):
    instance = species()
    instance.migrated = True
    instance.reset_migration()
    assert instance.migrated is False


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_birth_probability(species):
    """ Check that the birth function returns None if the weight of the animal is low"""
    species = species(3, 14)
    species.set_params({"zeta": 200})
    num_of_animal = 3
    birth_species = species.birth(num_of_animal)

    assert birth_species is None


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_birth_False(mocker, species):
    """Check that the animals cant reproduce if only
    one animal is present regardless of random value"""
    mocker.patch('random.random', return_value=1)
    species = species(5, 35)
    num_of_animal = 3

    birth_species = species.birth(num_of_animal)

    assert birth_species is None


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_birth_True(mocker, species):
    """ Check if the birth function returns True when the
    probability to give birth is les than the random value"""
    mocker.patch('random.random', return_value=0)
    animal = species(5, 35)
    num_of_animal = 3
    birth_species = animal.birth(num_of_animal)

    assert isinstance(birth_species, species)
    assert birth_species.age == 0


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_birth_num_animal_one(species):
    """ Check if the birth function returns None when the
    porbability to give birth is les than the random value"""
    species = species(5, 35)
    num_of_animal = 1

    birth_species = species.birth(num_of_animal)
    assert birth_species is None


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_mother_loose_weight(mocker, species):
    """ Check that the fitness changes with weight loss."""
    mocker.patch('random.gauss', return_value=40)
    animal = species(5, 35)
    num_of_animal = 10

    animal.birth(num_of_animal)
    assert animal.weight == 35


def test_fitness_herb_more_than_carni(mocker):
    """Check that the carnivore wont eat if the herbivore fitness is higher that the carnivores"""
    mocker.patch('random.random', return_value=0)
    carn = Carnivore(2, 5)
    herb_fitness = carn.fitness - 1

    assert carn.hunting_kill(herb_fitness)


def test_fitness_herb_more_than_carni_False(mocker):
    """Check that the carnivore wont eat if the herbivore fitness is higher that the carnivores"""
    mocker.patch('random.random', return_value=1)
    carn = Carnivore(2, 5)
    herb_fitness = carn.fitness - 1

    assert carn.hunting_kill(herb_fitness) is False


def test_fitness_herb_more_than_carni_zero():
    """Check that the carnivore wont eat if the herbivore fitness is higher that the carnivores"""
    carn = Carnivore(2, 5)
    herb_fitness = carn.fitness + 1
    assert carn.hunting_kill(herb_fitness) is False


def test_fitness_herb_more_than_carni_True(mocker):
    """Check that the carnivore wont eat if the herbivore fitness is higher that the carnivores"""
    mocker.patch('random.random', return_value=0)
    carn = Carnivore(2, 5)
    herb_fitness = carn.fitness - 1

    assert carn.hunting_kill(herb_fitness) is True


def test_hunting_fitness_difference_greater_than_DeltaPhiMax(mocker):
    """Test hunting difference between Carnivore and Herbivore fitness greater than DeltaPhiMax"""
    mocker.patch('random.random', return_value=0)
    Carnivore.set_params({"DeltaPhiMax": 0.001})
    carn = Carnivore(2, 5)
    herb_fitness = 0.01

    assert carn.hunting_kill(herb_fitness) is True


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
@pytest.mark.parametrize("omega_dict", [{"omega": 0.6}, {"omega": 0.4}])
def test_z_test_death(species, omega_dict):
    """
    Inspired by Hans Ekkehard Plesser to form this test:
    .../examples/biolab_project/tests/test_bacteria2.py

    A test that use the probabilistic test for death function:
    In this test, a 95 precent level of confidence is used, that gives
    the alhpa value as 0.05. Different values for omega is used too, to show
    the difference between the species. By using a low fitness, the omega values
    will be set over the death probability.

    For large N, the distribution is approximately normal (law of large numbers) with mean
    Np and variance Np(1-p). Then, Z = ( n - Np ) / sqrt( N p (1-p) ) is distributed according
    to the normal distribution with mean 0 and variance 1. Thus,
    if death() works correctly, we will observe Z < -Z* or Z > Z* only
    with probability Phi(-Z*) + ( 1-Phi(Z*) ) = 2 * Phi(-Z*), where
    equality follows from the symmetry of the normal distribution.
    This is the probability mass in the tails of the distribution.

    A signficance level alpha at 0.05 has been used, and
    pass the test if 2*Phi(-|Z|) > alpha: If the probability mass outside (-|Z], |Z]), the test
    will pass.

    """
    animal = species(ini_age=200, ini_weight=5)
    animal.set_params(omega_dict)
    p = species.params["omega"]

    num = 1000
    n = sum(animal.death() for _ in range(num))

    mean = num * p
    var = num * p * (1 - p)

    Z = (n - mean) / np.sqrt(var)
    phi = 2 * stats.norm.cdf(-abs(Z))
    assert phi > ALPHA


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
@pytest.mark.parametrize("emigration_dict", [{"mu": 0.05}])
def test_z_test_emigration(species, emigration_dict):
    """
    Inspired by Hans Ekkehard Plesser to form this test:
    .../examples/biolab_project/tests/test_bacteria2.py

    As the test above, this probabilistic test for emigration function:
    In this test, a 95 precent level of confidence is used, that gives
    the alhpa value as 0.05. Different values for omega is used too, to show
    the difference between the species. By using a low fitness, the omega values
    will be set over the death probability.

    For large N, the distribution is approximately normal (law of large numbers) with mean
    Np and variance Np(1-p). Then, Z = ( n - Np ) / sqrt( N p (1-p) ) is distributed according
    to the normal distribution with mean 0 and variance 1. Thus,
    if death() works correctly, we will observe Z < -Z* or Z > Z* only
    with probability Phi(-Z*) + ( 1-Phi(Z*) ) = 2 * Phi(-Z*), where
    equality follows from the symmetry of the normal distribution.
    This is the probability mass in the tails of the distribution.

    A signficance level alpha at 0.05 has been used, and
    pass the test if 2*Phi(-|Z|) > alpha: If the probability mass outside (-|Z], |Z]), the test
    will pass.

    """
    animal = species(ini_age=400, ini_weight=10)
    animal.set_params(emigration_dict)
    p = species.params["mu"]

    alpha = 0.01
    num = 100
    n = sum(animal.emigration() for _ in range(num))

    mean = num * p
    var = num * p * (1 - p)

    Z = (n - mean) / np.sqrt(var)
    phi = 2 * stats.norm.cdf(-abs(Z))
    assert phi > alpha
