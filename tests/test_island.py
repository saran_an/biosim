# -*- coding: utf-8 -*-

"""
Test set for Island class.

This set of tests checks of the Island class to be provided by
the simulation module of the biosim package.
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import pytest
import numpy as np
from biosim.island import Island
from biosim.landscape import Lowland, Water
import textwrap


def test_initiations_island_for_landscape():
    geogr = """\
               WWWW
               WLLW
               WWWW"""
    geogr = textwrap.dedent(geogr)
    ini_herbs = [{'loc': (2, 2), 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                                         for _ in range(5)]}]
    island = Island(island_map=geogr, ini_pop=ini_herbs)
    assert isinstance(island.island_map[(1, 1)], Water)
    assert isinstance(island.island_map[(2, 3)], Lowland)


def test_initiation_island_for_animals():
    """Tests that the island initiate when adding arguments geography and initial population."""
    num_herbivores = 5
    ini_herbs = [{'loc': (2, 2), 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                                         for _ in range(num_herbivores)]}]
    geogr = """\
               WWWW
               WLLW
               WWWW"""
    geogr = textwrap.dedent(geogr)
    island = Island(island_map=geogr, ini_pop=ini_herbs)
    num_herbivores_loc = island.island_map[(2, 2)].get_num_herbivores()
    assert island.initial_pop == ini_herbs and num_herbivores_loc == num_herbivores


def test_different_row_lengths():
    """
    Tests that geography with different line length raises a ValueError.
    """
    geogr = """\
               WWWW
               WHLLHW
               WWWW"""
    geogr = textwrap.dedent(geogr)
    with pytest.raises(ValueError):
        Island(island_map=geogr)


def test_invalid_landscape():
    """
    Tests that invalid landscape types in geography raises a ValueError
    """
    geogr = """\
               WWWW
               WllW
               WWWW"""
    geogr = textwrap.dedent(geogr)
    with pytest.raises(ValueError):
        Island(island_map=geogr)


def test_invalid_first_last_rows():
    """Tests that first and last line in geography not entirely of 'W', raises ValueErrror."""
    geogr = """\
               WWWW
               WLLW
               WHWW"""
    geogr = textwrap.dedent(geogr)
    with pytest.raises(ValueError):
        Island(island_map=geogr)


def test_invalid_first_last_element_in_rows():
    """Tests that first and last character in each line not 'W', raises ValueError."""

    geogr = """\
               WWWW
               LLLW
               WWWW"""
    geogr = textwrap.dedent(geogr)
    with pytest.raises(ValueError):
        Island(island_map=geogr)


class TestAddPopulationAndNumPopulation:
    """Tests that adding population to invalid locations raises ValueError,
    and adding additional population to the same location """

    @pytest.fixture(autouse=True)
    def default_geography_initial_herbivores(self):
        """Creates a default island with herbivores for tests in class"""
        self.num_init_herbs = 5
        self.loc = (2, 2)
        self.ini_herbs = [{'loc': self.loc, 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                                                    for _ in range(self.num_init_herbs)]}]
        geogr = """\
                       WWWW
                       WLLW
                       WWWW"""
        geogr = textwrap.dedent(geogr)
        self.island = Island(island_map=geogr, ini_pop=self.ini_herbs)

    def test_add_population_out_of_boundaries(self):
        """Tests that population added to a locations out of boundary raises ValueError"""
        add_carns = [{'loc': (0, 0), 'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20}
                                             for _ in range(5)]}]
        with pytest.raises(ValueError):
            self.island.add_population(add_carns)

    def test_add_population_in_inhabitable_locations(self):
        """Tests that population added to a inhabitable location raises ValueError"""
        add_carns = [{'loc': (1, 1), 'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20}
                                             for _ in range(5)]}]
        with pytest.raises(ValueError):
            self.island.add_population(add_carns)

    def test_add_additional_population_to_same_location(self):
        """Tests that population is added to the initial population in particular location"""
        num_add_herbs = 6
        add_herbs = [{'loc': self.loc, 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                                               for _ in range(num_add_herbs)]}]
        self.island.add_population(additional_pop=add_herbs)
        num_herbs = self.island.island_map[self.loc].get_num_herbivores()
        assert num_herbs == self.num_init_herbs + num_add_herbs

    def test_get_num_pr_species(self):
        """Tests that number of animals of the species on island
         is the same as the initial numbers of animals added"""
        num_add_carns = 8
        add_carns = [{'loc': (2, 3), 'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20}
                                             for _ in range(num_add_carns)]}]
        self.island.add_population(additional_pop=add_carns)
        species_num = self.island.get_num_pr_species()
        num_herbs = species_num['Herbivore']
        num_carns = species_num['Carnivore']
        assert num_herbs == self.num_init_herbs and num_carns == num_add_carns

    def test_get_num_population_on_island(self):
        """Tests that the total number of animal is the same
        as the sum of the number of the added animals"""
        num_add_carns = 8
        add_carns = [{'loc': (2, 3), 'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20}
                                             for _ in range(num_add_carns)]}]
        self.island.add_population(additional_pop=add_carns)
        assert self.island.get_num_population() == self.num_init_herbs + num_add_carns


class TestMigration:
    """ Tests animal migration"""

    @pytest.fixture(autouse=True)
    def default_geography_initial_population(self):
        """Creates a default island with herbivores and carnivores for tests in class"""
        self.num_herbs = 6
        self.num_carns = 4
        self.loc = (2, 3)
        self.ini_herbs = [{'loc': self.loc, 'pop': [{'species': 'Herbivore',
                                                     'age': 5,
                                                     'weight': 20}
                                                    for _ in range(self.num_herbs)]}]
        self.init_carns = [{'loc': self.loc, 'pop': [{'species': 'Carnivore',
                                                      'age': 5,
                                                      'weight': 20}
                                                     for _ in range(self.num_carns)]}]

        geogr = """\
                           WWWW
                           WLLW
                           WWWW"""
        geogr = textwrap.dedent(geogr)
        self.island = Island(island_map=geogr, ini_pop=self.ini_herbs)
        self.island.add_population(additional_pop=self.init_carns)

    def test_all_emigrating_to_next_habitable_location(self, mocker):
        """Tests that all animals emigrate to the next location when emigration is certain"""
        mocker.patch('numpy.random.choice', return_value=2)
        mocker.patch('random.random', return_value=0)

        current_cell = self.island.island_map[self.loc]

        self.island.migration(self.loc)
        pop_after = current_cell.get_num_herbivores() + current_cell.get_num_carnivores()

        next_loc = self.island.emigrating_location(self.loc)
        next_cell = self.island.island_map[next_loc]
        pop_next_cell = next_cell.get_num_herbivores() + next_cell.get_num_carnivores()

        assert pop_after == 0 and pop_next_cell == self.num_herbs + self.num_carns

    def test_no_emigration_to_next_inhabitable_location(self, mocker):
        """Test that no animals will emigrate to an inhabitable cell, when emigration is certain"""
        mocker.patch('numpy.random.choice', return_value=1)
        mocker.patch('random.random', return_value=0)

        current_cell = self.island.island_map[self.loc]
        pop_before = current_cell.get_num_herbivores() + current_cell.get_num_carnivores()
        self.island.migration(self.loc)
        pop_after = current_cell.get_num_herbivores() + current_cell.get_num_carnivores()

        next_loc = self.island.emigrating_location(self.loc)  # Fixed so the target is a water cell
        next_possible_cell = self.island.island_map[next_loc]

        assert next_possible_cell.habitable is False and pop_after == pop_before

    def test_emigrating_location_with_fixed_choice(self, mocker):
        """Tests that next possible location is as expected with fixed choice"""
        mocker.patch('numpy.random.choice', return_value=0)
        next_location = Island.emigrating_location((3, 3))
        assert next_location == (2, 3)

    def test_emigrating_location_gives_neighbour_locations(self):
        """Test that the next location for emigration is one of the four adjacent cells"""
        location = (3, 3)
        next_location = Island.emigrating_location(location)
        neighbours = [(2, 3), (4, 3), (3, 2), (3, 4)]
        assert next_location in neighbours

    def test_reset_migration(self, mocker):
        """Test that reset migration will change every animal's migration-status from True
         after emigrating to False when called upon"""
        mocker.patch('numpy.random.choice', return_value=2)
        mocker.patch('random.random', return_value=0)

        self.island.migration(self.loc)
        next_loc = self.island.emigrating_location(self.loc)
        next_cell = self.island.island_map[next_loc]

        self.island.reset_migration()
        herbi_reset_move = [herbi.migrated for herbi in next_cell.herbivores]
        carni_reset_move = [carni.migrated for carni in next_cell.carnivores]

        assert all(np.array(herbi_reset_move)) is False and all(np.array(carni_reset_move)) is False


class TestAnnualCycleSpeciesAttributes:
    """Section with tests for the annual cycle on island and method to get a dictionary with
    information about all the animals age, weight and fitness values"""

    @pytest.fixture(autouse=True)
    def default_geography_initial_population(self):
        """Creates a default island with herbivores and carnivores for tests in class"""
        self.num_herbs = 4
        self.age_herbs = 5
        self.weight_herbs = 6
        self.num_carns = 3
        self.age_carns = 4
        self.weight_carns = 5
        self.herbs = [{'loc': (2, 2),
                       'pop': [{'species': 'Herbivore',
                                'age': self.age_herbs,
                                'weight': self.weight_herbs}
                               for _ in range(self.num_herbs)]}]
        self.carns = [{'loc': (2, 3),
                       'pop': [{'species': 'Carnivore',
                                'age': self.age_carns,
                                'weight': self.weight_carns}
                               for _ in range(self.num_carns)]}]

        geogr = """\
                   WWWW
                   WHHW
                   WWWW"""
        geogr = textwrap.dedent(geogr)
        self.island = Island(island_map=geogr, ini_pop=self.herbs)
        self.island.add_population(additional_pop=self.carns)

    def test_island_season_cycle(self):
        """Tests that the annual cycle on island runs"""
        self.island.annual_cycle()

    def test_get_species_attributes_island_age_herbivores(self):
        """Tests that method gives a dictionary with right values and amount of values for 'age'"""
        herbs_attributes = self.island.species_attributes[0]
        herbs_ages = herbs_attributes['age']
        assert len(herbs_ages) == self.num_herbs and all(herbs_ages == self.age_herbs)

    def test_get_species_attributes_island_age_carnivores(self):
        """Tests that method gives a dictionary with right values and amount of values for 'age'"""
        carns_attributes = self.island.species_attributes[1]
        carns_ages = carns_attributes['age']
        assert len(carns_ages) == self.num_carns and all(carns_ages == self.age_carns)

    def test_get_species_attributes_island_weight_herbivores(self):
        """Tests that method gives a dictionary with right values and amount of values for
        'weight'"""
        herbs_attributes = self.island.species_attributes[0]
        herbs_weights = herbs_attributes['weight']
        assert len(herbs_weights) == self.num_herbs and all(herbs_weights == self.weight_herbs)

    def test_get_species_attributes_island_weight_carnivores(self):
        """Tests that method gives a dictionary with right values and amount of values for
        'weight'"""
        carns_attributes = self.island.species_attributes[1]
        carns_weight = carns_attributes['weight']
        assert len(carns_weight) == self.num_carns and all(carns_weight == self.weight_carns)

    def test_get_species_attributes_island_fitness_herbivores(self):
        """Tests that method gives a dictionary with right values and amount of values for
        'fitness'"""
        herbivore_fitness = self.island.island_map[(2, 2)].herbivores[0].fitness
        herbs_attributes = self.island.species_attributes[0]
        herbs_fitness = herbs_attributes['fitness']
        assert len(herbs_fitness) == self.num_herbs and all(herbs_fitness == herbivore_fitness)

    def test_get_species_attributes_island_fitness_carnivores(self):
        """Tests that method gives a dictionary with right values and amount of values for
        'fitness'"""
        carnivore_fitness = self.island.island_map[(2, 3)].carnivores[0].fitness
        carns_attributes = self.island.species_attributes[1]
        carns_fitness = carns_attributes['fitness']
        assert len(carns_fitness) == self.num_carns and all(carns_fitness == carnivore_fitness)
