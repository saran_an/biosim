# -*- coding: utf-8 -*-

"""
Test set for Simulation class.

This set of tests checks of the Simulation class to be provided by
the simulation module of the biosim package.
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import pytest
import os
import shutil

from biosim.animals import Herbivore, Carnivore
from biosim.simulation import BioSim


@pytest.fixture
def standard_map_and_population():
    geogr = """\
                   WWWWWWWWWWWWWWWWWWWWW
                   WWWWWWWWHWWWWLLLLLLLW
                   WHHHHHLLLLWWLLLLLLLWW
                   WHHHHHHHHHWWLLLLLLWWW
                   WHHHHHLLLLLLLLLLLLWWW
                   WHHHHHLLLDDLLLHLLLWWW
                   WHHLLLLLDDDLLLHHHHWWW
                   WWHHHHLLLDDLLLHWWWWWW
                   WHHHLLLLLDDLLLLLLLWWW
                   WHHHHLLLLDDLLLLWWWWWW
                   WWHHHHLLLLLLLLWWWWWWW
                   WWWHHHHLLLLLLLWWWWWWW
                   WWWWWWWWWWWWWWWWWWWWW"""

    num_herbs = 50
    num_carns = 20
    ini_herbs = [{'loc': (9, 9),
                  'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                          for _ in range(num_herbs)]}]
    ini_carns = [{'loc': (9, 9),
                  'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20}
                          for _ in range(num_carns)]}]
    standard_simulation = BioSim(island_map=geogr, ini_pop=ini_herbs, seed=789, vis_years=0)
    standard_simulation.add_population(population=ini_carns)
    return standard_simulation


def test_simulation_standard_map_and_years(standard_map_and_population):
    """Test that simulating standard map with population is working properly,
    with standard visualization every year"""
    years = 50
    standard_map_and_population.simulate(years)
    assert standard_map_and_population.year == years


def test_simulation_num_animals(standard_map_and_population):
    """Tests that property num animals gives expected amount of population"""
    num_herbs = 50
    num_carns = 20
    assert standard_map_and_population.num_animals == num_herbs + num_carns


@pytest.mark.parametrize('species, num_animals', [('Herbivore', 50), ('Carnivore', 20)])
def test_simulation_num_animals_per_species(standard_map_and_population, species, num_animals):
    """Tests that property num animals pr species gives expected amount of animals"""
    assert standard_map_and_population.num_animals_per_species[species] == num_animals


def test_simulation_img_years_not_multiple_of_vis_years():
    geogr = """\
               WWWWWWWWWWWWWWWWWWWWW
               WLLLLLLLLLLLLLLLLLLLW
               WLLLLLLLLLLLLLLLLLLLW
               WLLLLLLLLLLLLLLLLLLLW
               WLLLLLLLLLLLLLLLLLLLW
               WLLLLLLLLLLLLLLLLLLLW
               WWWWWWWWWWWWWWWWWWWWW"""

    ini_herbs = [{'loc': (3, 3),
                  'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                          for _ in range(10)]}]
    with pytest.raises(ValueError):
        sim = BioSim(island_map=geogr, ini_pop=ini_herbs, seed=789, vis_years=2, img_years=3)
        sim.simulate(10)


def test_simulation_make_log_file():
    """
    Tests that log file is obtaining information about animal counts in island when given.
    Created log_file is removed after test.
    Removal of file is inspired from:
        https://stackoverflow.com/questions/6996603/how-to-delete-a-file-or-folder-in-python
    """
    geogr = """\
                   WWWW
                   WLLW
                   WWWW"""

    ini_herbs = [{'loc': (2, 2), 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                                         for _ in range(5)]}]
    log_file = 'herbivore_count.csv'
    sim = BioSim(island_map=geogr, ini_pop=ini_herbs, seed=789, vis_years=0,
                 log_file=log_file)
    sim.simulate(10)

    if os.path.isfile(log_file):
        os.remove(log_file)
    else:
        print(f'{log_file} is not found!')


def test_simulation_make_movie():
    """
    Tests that movie is created the called upon.
    Created images and movies is removed after test
    Removal of directory with content is inspired from:
        https://stackoverflow.com/questions/6996603/how-to-delete-a-file-or-folder-in-python
    """
    geogr = """\
               WWWW
               WLLW
               WWWW"""

    ini_herbs = [{'loc': (2, 2), 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20}
                                         for _ in range(50)]}]
    ini_carns = [{'loc': (2, 2), 'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20}
                                         for _ in range(5)]}]

    img_dir = 'results'
    sim = BioSim(geogr, ini_herbs + ini_carns, seed=789,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 60.0, 'delta': 2},
                             'weight': {'max': 60, 'delta': 2}},
                 cmax_animals={'Herbivore': 200, 'Carnivore': 50},
                 img_dir=img_dir,
                 img_base='sample')
    sim.simulate(10)
    sim.make_movie()
    try:
        shutil.rmtree(img_dir)
    except OSError as e:
        print(f'Error {e.filename} {e.strerror}')


@pytest.mark.parametrize('animal, species, extra',
                         [(Herbivore, 'Herbivore', {}),
                          (Carnivore, 'Carnivore', {'DeltaPhiMax': 0.5})])
def test_set_param_animals(standard_map_and_population, animal, species, extra):
    """Tests that parameters can be set on animal classes"""

    params = {'w_birth': 7.0,
              'sigma_birth': 1.5,
              'beta': 0.9,
              'eta': 0.05,
              'a_half': 40.,
              'phi_age': 0.2,
              'w_half': 10.,
              'phi_weight': 0.1,
              'mu': 0.25,
              'gamma': 0.2,
              'zeta': 3.5,
              'xi': 1.2,
              'omega': 0.4,
              'F': 10.}
    params.update(extra)
    standard_map_and_population.set_animal_parameters(species, params)
    assert animal.get_params() == params


def test_set_param_invalid_species(standard_map_and_population):
    """Tests that setting parameters for invalid species, raises ValueError"""
    with pytest.raises(ValueError):
        standard_map_and_population.set_animal_parameters('Bear', {'w_birth': 8.0})


@pytest.mark.parametrize('lscape, params',
                         [('L', {'f_max': 100.}),
                          ('H', {'f_max': 200.})])
def test_set_param_landscape(standard_map_and_population, lscape, params):
    """Parameters can be set on landscape classes"""
    standard_map_and_population.set_landscape_parameters(lscape, params)
