# -*- coding: utf-8 -*-

"""
:mod: `sample_sim_log` is a small demo script running a BioSim
       simulation that make use of the ability to store the animal counts
       throughout the simulation to a file, csv is preferred. The file name
       should have the csv format, even though it does not exist already in current
       directory.
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import textwrap
from biosim.simulation import BioSim

if __name__ == '__main__':

    geogr = """\
               WWWWWWWWWWWWWWWWWWWWW
               WHHHHDDDDDDHHHHHHHWWW
               WWHHHHDDDDDDLLLLHHHWW
               WWHHHHDDDLLLLLHHHWWWW
               WHHHHHDDDDDLLLLLHHHWW
               WHHHHDDDDDDLLLLLHHHWW
               WWHHHHDDDDDLLLLLHWWWW
               WWWHHHHLLLLLLLHHHWWWW
               WWWHHHHHHDDDDDDWWWWWW
               WWWWWWWWWWWWWWWWWWWWW"""
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (7, 13),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(100)]}]

    ini_carns = [{'loc': (7, 14),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(25)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs + ini_carns, seed=1,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 60.0, 'delta': 2},
                             'weight': {'max': 60, 'delta': 2}},
                 cmax_animals={'Herbivore': 200, 'Carnivore': 50},
                 log_file='animal_count.csv')
    sim.simulate(100)
